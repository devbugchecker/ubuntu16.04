#!/bin/bash
    echo "Enter the VirtualHost name : "
    read name
    echo "Enter the VirtualHost root [/var/www/html/] : "
    read root
 
if [ -d "/var/www/html/"$root ]
    then
    if [ -w /etc/hosts ]
        echo "creating apache virtial host..."
        then
                    if [ -w /etc/apache2/sites-available ]
                        then
                                printf "<VirtualHost *:8080>
                                    ServerAdmin webmaster@localhost
                                    DocumentRoot /var/www/html/$root
                                    ServerName  $name
                                    ServerAlias $name
                                     <Directory /var/www/html/$root>
                                        AllowOverride None
                                        Order allow,deny
                                        Allow from all
                                       Require all granted
                                         Header set Access-Control-Allow-Origin '"*"'

                                     </Directory>
                                      Alias /storage '"/var/www/html/$root"'

                                    ErrorLog ${APACHE_LOG_DIR}/error.log
                                    CustomLog ${APACHE_LOG_DIR}/access.log combined
                                    </VirtualHost>" >> /etc/apache2/sites-available/$name.conf
                                    echo "adding site to hosts file..."
                                        #printf "127.0.0.1   $name.dev\n" >> /etc/hosts
                                        sudo a2ensite "$name.conf"
                                        sudo service apache2 restart
                                        sudo service apache2 reload
                                else
                                        echo "You do not have pemission to write to the apache folder , try using sudo."
                                fi
                else
                        echo "You do not have pemission to write to the $root folder , try using sudo."
                fi
else
    echo "Directory $root does not exist!"
fi